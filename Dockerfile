FROM nginx:alpine
MAINTAINER Marty Oehme <marty.oehme@gmail.com>

COPY public /usr/share/nginx/html
