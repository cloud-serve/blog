+++
date = "2017-06-24T18:57:12+02:00"
title = "About"
menu = "main"
+++

My name is Marty, this blog is just for fun.
Also, it exists to allow me to explore static site generators - and hugo seems
an awesome first choice.

---

## What Is This About

Hopefully, in the future, there should be a few blog entries showing you stuff
I did and how I accomplished it (or how I didn't). Then this section should really
not be necessary anymore - we'll see. Have fun reading :)
