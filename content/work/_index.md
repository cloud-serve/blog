+++
title = "Work"
date = "2017-07-02T08:15:25+07:00"
+++

Floyd-Steinberg Dithering Algorithm - [Blog Post](/2018/02/19/) and [Source Code](https://gitlab.com/marty.oehme/floyd-steinberg-dithering)
